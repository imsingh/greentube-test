# Greentube Angular2 Test

* `npm install` and then `ng serve`
* Then go to `http://localhost:4200/` to see it working.
* You need angular-cli for using this project.
* I have combined Part 2 and Part 3 of test in this single app.
* Angular2 Material is used along with angular2.
* Custom Validator for validating Email is borrowed from Thougthram team.

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.24.
