import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  user:any = {};
  constructor(public snackBar: MdSnackBar) { }

  ngOnInit() {
  }

  login(form) {
  	this.snackBar.open("Successfully Logged In", null , {duration:1500});
  	this.user = {};
  	form.reset();
  }

}
